$(document).ready(function() {
	var $form = $('form.sendMsg');

	// var messageHandler = function() {

	// }

	$form.submit(function(e) {		
		// Error Check Fields


		console.log("ajaxing request");
		// Send data to server
		$.ajax({			
			url: '/textMeNow',
			type: 'POST', 
			cache: false, 
        	data: $form.serialize(),
			// Receive response from server and do stuff
			success: function(response) {				
				result = response;
				$(".alert-placeholder").html ('<div class="alert alert-dismissable ' + result.alert + '"></div>')	// Adds alert message
				$(".alert").append( '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + result.msg ); // Appends close button
				if (result.success === 0) {
					console.log("success!")
					$form.find('input.btn').prop('disabled', true);
				};
			}     
		});


		return false;
	});
});
