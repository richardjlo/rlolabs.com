$( document ).ready(function() {
	$('form.bmi-calc').submit(function(e) {

		// Get values from from
		var heightFt = $('#heightFt').val();
		var heightIn = $('#heightIn').val();
		var weight = $('#weight').val();

		// Send data to server
		$.ajax({
			url: '/bmi',
			type: 'POST', 
			cache: false,
			data: {
				heightFt: heightFt,
				heightIn: heightIn,
				weight: weight
			}, 

			// Receive response from server and do stuff
			success: function(response) {				
				$('div#result').html("<h3>Your BMI: <span class='highlight'>" + response.result + "</span><br/> Category: <span class='highlight'>" + response.bmiCategory + "</span></h3>");
			}
		});
		return false;

	});
});
