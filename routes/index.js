var client = require('twilio')('AC69a107a777537c0e639e246733287fd4', '91f773a0603cce6bf4745b550e5f4997');

exports.index = function(req, res){
  res.render('index', { title: 'rlo labs' });
};

exports.bmi = function (req, res) {
	res.render('bmi', { title: 'BMI Calculator' });
};

exports.textMeNow = function (req, res) {
	res.render('textMeNow', { title: 'Text Me Now' });
};

exports.scheduleMsg = function (req, res) {
	// TODO for text me later
};

exports.sendMsg = function (req, res) {
	// console.log(req.body);

	//Send an SMS text message
	client.sendSms({

	    // to:'+18659240469', // Any number Twilio can deliver to
	    to: '+1' + req.body.number,
	    from: '+16504667744', // A number you bought from Twilio and can use for outbound communication
	    body: req.body.message // body of the SMS message

	}, function(err, responseData) { //this function is executed when a response is received from Twilio

	    if (!err) { // "err" is an error received during the request, if any

	        // "responseData" is a JavaScript object containing data received from Twilio.
	        // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
	        // http://www.twilio.com/docs/api/rest/sending-sms#example-1

	        console.log(responseData.from); // outputs "+14506667788"
	        console.log(responseData.body); // outputs "word to your mother."
	        res.send({ success: 0, alert: 'alert-success' , msg: 'Great Success! Your message was sent :) '});			
	        // console.log("alert sent");

	    } else {
	    	console.log("Error!");
	    		// Send data back to ajax function
			// res.send({ 
			// 	result: "error"				
			// });	
			res.send({ success: 1,  alert: 'alert-danger' , msg: 'Looks like something is wrong. Please check your number and try again. '});			
	    };
	});	

};

exports.bmiCalc = function (req, res) {	

	// Receive data from ajax function	
	var heightFt = parseFloat(req.body.heightFt);	
	var heightIn = parseFloat(req.body.heightIn);
	var weight = parseFloat(req.body.weight);
	
	// Calculate result & BMI Category
	var totalHeight = (heightFt * 12) + heightIn;	
	var result = (weight/Math.pow(totalHeight,2)) * 703;	
	result=Math.round(result*10)/10;

	var bmiCategory;
	if (result) {
		if (result < 18.5) {
			bmiCategory = "Underweight";
		} else if (result < 25) {
			bmiCategory = "Normal";
		} else if (result < 30) {
			bmiCategory = "Overweight";
		} else if (result >= 30) {
			bmiCategory = "Obese";
		};	
	} else {	
		result = "error";
		bmiCategory = "error";
	}	

	// Send data back to ajax function
	res.send({ 
		result: result, 
		bmiCategory: bmiCategory
	});	

	// //Send an SMS text message
	// client.sendSms({

	//     to:'+18659240469', // Any number Twilio can deliver to
	//     from: '+18657304906', // A number you bought from Twilio and can use for outbound communication
	//     body: "Your BMI is: " + result + ". That makes you " + bmiCategory // body of the SMS message

	// }, function(err, responseData) { //this function is executed when a response is received from Twilio

	//     if (!err) { // "err" is an error received during the request, if any

	//         // "responseData" is a JavaScript object containing data received from Twilio.
	//         // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
	//         // http://www.twilio.com/docs/api/rest/sending-sms#example-1

	//         console.log(responseData.from); // outputs "+14506667788"
	//         console.log(responseData.body); // outputs "word to your mother."

	//     }
	// });	
};