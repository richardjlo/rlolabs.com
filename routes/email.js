// Requirements and API Keys
var Parse = require('node-parse-api').Parse;
var async = require('async');

var APP_ID = 'uDcaPLGKgXDp4nxxFuOnIiVjUKgJYXHXh9IOSX5C';
var MASTER_KEY = 'ae8ap0qHorbiJbPX9NWizzNmt91SoqhxuaARHlYz';

var app = new Parse(APP_ID, MASTER_KEY);

exports.emailSubscription = function (req, res) {
	res.render('emailSubscription', { title: 'Subscribe to our mailing list'});
}

exports.saveEmail = function (req, res) {
	var createUser = function(userEmail) {
		app.insert('UserObject', { email: userEmail }, function(err, resonse) {	
		});
	}	

	var subscribeUser = function(userEmail) {
		async.waterfall([
		    function(callback){        
		    	// Search database for user
				app.find('UserObject', { email: userEmail }, function (err, response) {
					var userExists = false;
					if (response.results[0]) {
						userExists = true;
					}				
					callback(null, userExists, userEmail);
				});                
		    },
		    // If user exists, notify user. Otherwise, create new account
		    function(userExists, userEmail, callback){
		    	var alert;
		    	var msg;
		    	var success;
		    	if (!userExists) {
		    		// console.log("creating new user");
		    		createUser(userEmail);	 		
		    		alert = 'alert-success';
		    		msg = "Thanks for subscribing!";
		    		success = 0;
		    	} else {
		    		// console.log("sorry, this email already exists");
		    		alert = 'alert-danger';
		    		msg = "Seems like you're on the list. Try a different email?";
		    		success = 1;
		    	};		    	

	    		res.send({
	    			'alert' : alert,
	    			'msg' : msg,
	    			'success' : success
	    		});		    	

		        callback(null);
		    }, 
		], function (err, result) {
		   // result now equals 'done'    
		});	

	}

	subscribeUser(req.body.email);
	
}












